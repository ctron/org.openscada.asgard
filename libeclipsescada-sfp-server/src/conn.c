/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#include "config.h"

int
_sfp_check_long_start (struct evbuffer *input, sfp_bool is_start)
{
  unsigned char * mem = evbuffer_pullup (input, 6);
  uint16_t len;
  memcpy (&len, mem + 3, sizeof(uint16_t));
  if (is_start)
    {
      len = be16toh(len);
    }

  if (evbuffer_get_length (input) < (size_t) (len + 3))
    {
      DEBUG("Data missing (len=%d, have=%ld)", len + 3,
	  evbuffer_get_length (input));
      return -1;
    }

  // we have all the data we need
  evbuffer_drain (input, 5); // drain

  return len;
}

int
_sfp_connection_variant_decode (struct evbuffer *input,
				struct sfp_variant * variant)
{
  uint8_t data_type;
  evbuffer_remove (input, &data_type, sizeof(uint8_t));
  switch (data_type)
    {
    case VT_DEAD:
      sfp_variant_set_dead (variant);
      return 0;
    case VT_NULL:
      sfp_variant_set_null (variant);
      return 0;
    case VT_INT32:
      {
	int32_t value;
	if (evbuffer_remove (input, &value, sizeof(int32_t)) != sizeof(int32_t))
	  return -1;

	sfp_variant_set_int32 (variant, value);
	return 0;
      }
    case VT_INT64:
      {
	int64_t value;
	if (evbuffer_remove (input, &value, sizeof(int64_t)) != sizeof(int64_t))
	  return -1;

	sfp_variant_set_int64 (variant, value);
	return 0;
      }
    case VT_BOOLEAN:
      {
	uint8_t value;
	if (evbuffer_remove (input, &value, sizeof(uint8_t)) != sizeof(uint8_t))
	  return -1;

	sfp_variant_set_bool (variant, value != 0x00);
	return 0;
      }
    case VT_DOUBLE:
      {
	double value;
	if (evbuffer_remove (input, &value, sizeof(double)) != sizeof(double))
	  return -1;

	sfp_variant_set_double (variant, value);
	return 0;
      }
    case VT_STRING:
      {
	char * string;
	int rc;

	rc = _sfp_proto_decode_string (input, sfp_false, &string);
	if (rc < 0)
	  return rc;

	if (string == NULL)
	  {
	    sfp_variant_set_string (variant, "");
	  }
	else
	  {
	    sfp_variant_set_string_allocated (variant, string);
	  }
	return 0;
      }

    default:
      return -1;
    }
}

int
_sfp_process_write_command (struct sfp_connection * connection,
			    struct evbuffer *input)
{
  DEBUG("Processing WRITE_COMMAND");

  int len = _sfp_check_long_start (input, sfp_false);
  if (len < 0)
    return 0; // we need to wait for data, this is not an error

  int rc;

  struct sfp_message_write_command message;
  evbuffer_remove (input, &message.register_number, sizeof(uint16_t));
  evbuffer_remove (input, &message.operation_id, sizeof(int32_t));
  struct sfp_variant value;

  rc = _sfp_connection_variant_decode (input, &value);
  if (rc < 0)
    {
      DEBUG("Failed to decode variant: %d", rc);
      return rc;
    }

  message.value = &value;

  connection->strategy->handle_write_command (connection->strategy, &message);

  DEBUG("Size after WRITE_COMMAND: %ld", evbuffer_get_length (input));

  return 0;
}

void
_sfp_process_hello (struct sfp_connection * connection, struct evbuffer *input)
{
  DEBUG("Processing HELLO");

  int len = _sfp_check_long_start (input, sfp_true);
  if (len < 0)
    return;

  // parse
  struct sfp_message_hello hello;
  evbuffer_remove (input, &hello.version, 1);
  evbuffer_remove (input, &hello.nodeId, 2);
  evbuffer_remove (input, &hello.features, 2);
  hello.nodeId = be16toh(hello.nodeId);
  hello.features = be16toh(hello.features);

  DEBUG("HELLO - version: %02hhx, nodeId: %02hx, features: %02hx",
      hello.version, hello.nodeId, hello.features);

  connection->strategy->handle_hello (connection->strategy, &hello);

  DEBUG("Size after HELLO: %ld", evbuffer_get_length (input));
}

int
_sfp_connection_process_message (struct sfp_connection * connection,
				 struct evbuffer *input)
{
  unsigned char * mem;

  mem = evbuffer_pullup (input, 3);
  uint8_t cc = mem[2];
  DEBUG("Command code: 0x%02hhx", cc);

  switch (cc)
    {
    case MC_HELLO:
      {
	_sfp_process_hello (connection, input);
	return 0;
      }
    case MC_READ_ALL:
      {
	evbuffer_drain (input, 3);
	DEBUG("Size after READ ALL: %ld", evbuffer_get_length (input));
	connection->strategy->handle_read_all (connection->strategy);
	return 0;
      }
    case MC_START_BROWSE:
      {
	evbuffer_drain (input, 3);
	DEBUG("Size after START BROWSE: %ld", evbuffer_get_length (input));
	connection->strategy->handle_start_browse (connection->strategy);
	return 0;
      }
    case MC_STOP_BROWSE:
      {
	evbuffer_drain (input, 3);
	DEBUG("Size after STOP BROWSE: %ld", evbuffer_get_length (input));
	connection->strategy->handle_stop_browse (connection->strategy);
	return 0;
      }
    case MC_WRITE_COMMAND:
      {
	return _sfp_process_write_command (connection, input);
      }
    default:
      {
	DEBUG("Unknown command code: 0x%02hhx", cc);
	return -2;
      }
    }
}

void
_sfp_connection_read_cb (struct bufferevent *bev, void *ctx)
{
  struct sfp_connection * connection = (struct sfp_connection *) ctx;

  struct evbuffer *input = bufferevent_get_input (bev);

  DEBUG("start Buffer len: %ld", evbuffer_get_length (input));

  unsigned char * mem;

  // looking for the start
  uint16_t start;
  do
    {
      if (evbuffer_get_length (input) < 2)
	return; // need more data

      mem = evbuffer_pullup (input, 2);
      memcpy (&start, mem, sizeof(uint16_t)); // peek
      start = be16toh(start);
      if (start != 0x1202)
	{
	  //we can eat garbage
	  evbuffer_drain (input, 2);
	}DEBUG("Buffer len: %ld", evbuffer_get_length (input));
    }
  while (start != 0x1202);

  DEBUG("After - Buffer len: %ld", evbuffer_get_length (input));

  if (evbuffer_get_length (input) < 3)
    {
      return; // wait for more data
    }

  int rc = _sfp_connection_process_message (connection, input);
  if (rc < 0)
    {
      INFO("Protocol error: %d", rc);
      _sfp_connection_free (connection);
    }
}

void
sfp_connection_event_cb (struct bufferevent *bev, short events, void *ctx)
{
  struct sfp_connection * connection = (struct sfp_connection *) ctx;
  if (events & BEV_EVENT_ERROR)
    {
      INFO("Error from bufferevent: %s", strerror(errno));
      _sfp_connection_free (connection);
      return;
    }

  if (events & (BEV_EVENT_TIMEOUT))
    {
      INFO("Connection timed out");
      _sfp_connection_free (connection);
      return;
    }

  if (events & (BEV_EVENT_EOF | BEV_EVENT_ERROR))
    {
      _sfp_connection_free (connection);
    }
}

struct sfp_connection *
_sfp_connection_new (struct sfp_server * server, evutil_socket_t fd)
{
  DEBUG("New connection");

  struct sfp_connection * connection = calloc (sizeof(struct sfp_connection),
					       1);

  struct timeval read_timeout =
    { 10, 0 };

  /* We got a new connection! Set up a bufferevent for it. */
  struct bufferevent *bev = bufferevent_socket_new (server->evbase, fd,
						    BEV_OPT_CLOSE_ON_FREE);

  bufferevent_setcb (bev, _sfp_connection_read_cb, NULL,
		     sfp_connection_event_cb, (void*) connection);
  bufferevent_enable (bev, EV_READ | EV_WRITE);

  bufferevent_set_timeouts (bev, &read_timeout, NULL);

  connection->bev = bev;
  connection->evbase = server->evbase;
  connection->strategy = sfp_strategy_new (server, connection);

  return connection;
}

void
_sfp_connection_free (struct sfp_connection * connection)
{
  DEBUG("Closing connection");

  if (!connection)
    return;

  sfp_strategy_free (connection->strategy);

  bufferevent_free (connection->bev);

  free (connection);

  DEBUG("Closed connection");
}
