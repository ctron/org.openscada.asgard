/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#include "config.h"

void
_sfp_strategy_readall_read_all (struct sfp_strategy * strategy);
void
_sfp_strategy_readall_handle_hello (struct sfp_strategy * strategy,
				    struct sfp_message_hello * hello);
void
_sfp_strategy_readall_handle_start_browse (struct sfp_strategy * strategy);

int counter = 0;

void
_sfp_strategy_readall_read_all (struct sfp_strategy * strategy)
{
  struct sfp_registry * registry = strategy->server->registry;

  if (registry->items_count <= 0)
    return;

  struct sfp_data_update_entry * entries = calloc (
      registry->items_count, sizeof(struct sfp_data_update_entry));

  counter++;

  // gather updates

  size_t pos = 0;
  for (size_t i = 0; i < registry->items_space; i++)
    {
      if (!registry->items[i])
	{
	  continue;
	}
      entries[pos].register_number = registry->items[i]->register_number;
      entries[pos].timestamp = registry->items[i]->timestamp;
      entries[pos].value = registry->items[i]->value;
      pos++;
    }

  // send updates

  _sfp_proto_send_data_updates (strategy, entries, registry->items_count);

  free (entries);
}

#define SFP_FEATURES_LITTLE_ENDIAN (1<<1)

void
_sfp_strategy_readall_handle_hello (struct sfp_strategy * strategy,
				    struct sfp_message_hello * hello)
{
  // encode welcome

  struct evbuffer *data = evbuffer_new ();

  uint16_t features = 0;

#ifdef SFP_LITTLE_ENDIAN
  features |= SFP_FEATURES_LITTLE_ENDIAN; // LITTLE ENDIAN
  DEBUG("Feature: LITTLE_ENDIAN");
#endif

  DEBUG("All features: 0x%04hx (BE: 0x%04hx)", features, htons (features));
  features = htons (features);
  evbuffer_add (data, &features, sizeof(uint16_t));

  uint16_t propertiesCount = htons (0);
  evbuffer_add (data, &propertiesCount, sizeof(uint16_t));

  _sfp_proto_send_long_message (strategy->connection, 0x02, data, sfp_true);
}

void
_sfp_strategy_readall_namespace_listener_cb (
    sfp_registry_ns_event_type event_type, struct sfp_item**items, size_t count,
    void * ctx)
{
  struct sfp_strategy * strategy = (struct sfp_strategy*) ctx;

  DEBUG("Namespace update: %d", (int )event_type);

  for (size_t i = 0; i < count; i++)
    {
      DEBUG("\tItem - register_number: %04hd, id: %s",
	  items[i]->register_number, items[i]->item_id);
    }

  switch (event_type)
    {
    case ITEMS_ADDED:
      _sfp_proto_send_ns_added (strategy, items, count);
      break;

    case ITEMS_REMOVED:
      /* We don't send out any notification here, since the next
       * get data cycle will deliver this information due to the missing
       * item. Other strategies will handle this differently.
       */
      break;

    default:
      ERROR("Unknown event type: %d", (int )event_type);
      break;
    }

}

void
_sfp_strategy_readall_handle_start_browse (struct sfp_strategy * strategy)
{
  sfp_registry_add_namespace_listener (
      strategy->server->registry, _sfp_strategy_readall_namespace_listener_cb,
      strategy);
}

void
_sfp_strategy_readall_handle_stop_browse (struct sfp_strategy * strategy)
{
  sfp_registry_remove_namespace_listener (
      strategy->server->registry, _sfp_strategy_readall_namespace_listener_cb,
      strategy);
}

void
_sfp_strategy_readall_dispose (struct sfp_strategy * strategy)
{
  DEBUG("Disposing");

  sfp_registry_remove_namespace_listener (
      strategy->server->registry, _sfp_strategy_readall_namespace_listener_cb,
      strategy);
}

void
sfp_strategy_readall_init (struct sfp_strategy * strategy)
{
  strategy->handle_hello = _sfp_strategy_readall_handle_hello;
  strategy->handle_read_all = _sfp_strategy_readall_read_all;
  strategy->handle_start_browse = _sfp_strategy_readall_handle_start_browse;
  strategy->handle_stop_browse = _sfp_strategy_readall_handle_stop_browse;
  strategy->handle_dispose = _sfp_strategy_readall_dispose;
}
