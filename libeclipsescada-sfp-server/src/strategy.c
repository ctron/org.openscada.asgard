/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#include "config.h"

void
_sfp_strategy_write_handler_result (
    struct sfp_strategy* strategy, int32_t operation_id,
    struct sfp_error_information * error_information)
{
  DEBUG("Write complete - operation_id: %d", operation_id);

  if (error_information == NULL)
    {
      _sfp_proto_send_write_result (strategy, operation_id, SFP_ERROR_OK, NULL);
    }
  else
    {
      _sfp_proto_send_write_result (strategy, operation_id,
				    error_information->error_code,
				    error_information->error_message);
      sfp_error_information_free (error_information);
    }
}

void
_sfp_strategy_write_handler (struct sfp_strategy* strategy,
			     struct sfp_message_write_command* message)
{
  DEBUG("Handle write command - %04hd, operation_id: %d",
	message->register_number, message->operation_id);

  sfp_registry_lock_read (strategy->server->registry);

  struct sfp_item * item;

  item = sfp_registry_find_by_register (strategy->server->registry,
					message->register_number);

  if (item == NULL)
    {
      DEBUG("Item not found");
      _sfp_proto_send_write_result (strategy, message->operation_id,
				    SFP_ERROR_WRITE_NO_ITEM, NULL);
      sfp_registry_unlock (strategy->server->registry);
      return;
    }

  if (item->handle_write == NULL)
    {
      DEBUG("Item has no write handler");
      _sfp_proto_send_write_result (strategy, message->operation_id,
				    SFP_ERROR_WRITE_NO_HANDLER, NULL);
      sfp_registry_unlock (strategy->server->registry);
      return;
    }

  item->handle_write (item, message->value, message->operation_id, strategy,
		      _sfp_strategy_write_handler_result,
		      item->handle_write_ctx);
  sfp_registry_unlock (strategy->server->registry);
}

struct sfp_strategy *
sfp_strategy_new (struct sfp_server * server,
		  struct sfp_connection * connection)
{
  struct sfp_strategy *strategy = calloc (1, sizeof(struct sfp_strategy));
  strategy->connection = connection;
  strategy->server = server;

  strategy->handle_write_command = _sfp_strategy_write_handler;
  sfp_strategy_readall_init (strategy);

  return strategy;
}

void
sfp_strategy_free (struct sfp_strategy *strategy)
{
  if (strategy->handle_dispose)
    {
      strategy->handle_dispose (strategy);
    }
  free (strategy);
}
