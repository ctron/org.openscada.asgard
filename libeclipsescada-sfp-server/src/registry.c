/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#include "config.h"

/*
 * Private methods
 */

uint16_t
_sfp_registry_add_item (struct sfp_registry * registry, struct sfp_item * item);
int32_t
_sfp_registry_remove_item (struct sfp_registry * registry,
			   struct sfp_item * item);
void
_sfp_item_updated (struct sfp_item * item);

void
_sfp_registry_fire_ns_listeners (struct sfp_registry * registry,
				 sfp_registry_ns_event_type event_type,
				 struct sfp_item ** items, size_t numitems);

/*
 * Implementations
 */

struct sfp_registry *
sfp_registry_new ()
{
  struct sfp_registry * registry = calloc (1, sizeof(struct sfp_registry));

  registry->_ns_listeners = calloc (1,
				    sizeof(struct _sfp_registry_ns_listener));
  registry->_ns_listeners_count = 0;
  registry->_ns_listeners_space = 1;

  return registry;
}

void
sfp_registry_free (struct sfp_registry * registry)
{
  if (!registry)
    return;

  free (registry->_ns_listeners);
  free (registry);
}

sfp_bool
_sfp_registry_has_item (struct sfp_registry * registry, const char * item_id)
{
  // TODO: should be implemented some way else then plain string compare

  if (registry->items_count <= 0)
    return sfp_false;

  for (size_t i = 0; i < registry->items_space; i++)
    {
      struct sfp_item * item = registry->items[i];

      if (item == NULL)
	continue;

      if (item->item_id == NULL)
	continue;

      if (strcmp (item->item_id, item_id) == 0)
	return sfp_true; // found item id
    }

  return sfp_false;
}

struct sfp_item *
sfp_registry_register_item (struct sfp_registry * registry,
			    const char * item_id, const char * description,
			    const char * unit)
{
  if (item_id != NULL && _sfp_registry_has_item (registry, item_id))
    {
      // item id already registered, item IDs must be unique
      return NULL;
    }

  struct sfp_item * item = calloc (1, sizeof(struct sfp_item));

  item->registry = registry;
  item->item_id = item_id == NULL ? NULL : strdup (item_id);
  item->description = description == NULL ? NULL : strdup (description);
  item->unit = unit == NULL ? NULL : strdup (unit);
  item->value = sfp_variant_new_null ();

  int32_t register_number;
  register_number = _sfp_registry_add_item (registry, item);

  if (register_number < 0)
    {
      free (item);
      return NULL;
    }
  else
    {
      item->register_number = register_number;
      struct sfp_item * update[1];
      update[0] = item;
      _sfp_registry_fire_ns_listeners (registry, ITEMS_ADDED, update, 1);
      return item;
    }
}

void
sfp_registry_unregister_item (struct sfp_item * item)
{
  if (!item)
    return;

  // remove item from the internal list

  struct sfp_registry * registry = item->registry;

  _sfp_registry_remove_item (item->registry, item);

  // fire namespace updates
  struct sfp_item * update[1];
  update[0] = item;
  _sfp_registry_fire_ns_listeners (registry, ITEMS_REMOVED, update, 1);

  // free item information

  free (item->description);
  free (item->item_id);
  free (item->unit);

  // free item value

  sfp_variant_free (item->value);

  // free main

  free (item);
}

uint16_t
_sfp_registry_add_item (struct sfp_registry * registry, struct sfp_item * item)
{
  if (registry->items_count + 1 > registry->items_space)
    {
      // need to allocate
      if (registry->items_space == 0)
	{
	  // allocate the first
	  registry->items_space = 1;
	  registry->items = calloc (registry->items_space,
				    sizeof(struct sfp_item));
	}
      else
	{
	  // re-allocate more
	  registry->items_space = (registry->items_space * 1.5) + 1;
	  registry->items = realloc (
	      registry->items, sizeof(struct sfp_item) * registry->items_space);
	}

      // add item at the end
      uint32_t idx = registry->items_count;
      registry->items_count++;

      // add
      registry->items[idx] = item;

      DEBUG ( "Added new item (%s) at %d", item->item_id, idx);

      // return
      return idx;
    }
  else
    {
      // need to find a slot
      for (size_t i = 0; i < registry->items_space; i++)
	{
	  if (registry->items[i] == NULL)
	    {
	      registry->items[i] = item;
	      registry->items_count++;
	      return i;
	    }
	}
      // TODO: WHOOOOPSIEE("There should have been an empty space");
      return -1;
    }
}

int32_t
_sfp_registry_remove_item (struct sfp_registry * registry,
			   struct sfp_item * item)
{
  /* please let's not talk about performance for now
   * we could use the register number from the item.
   * But do we trust it for a direct access?
   */

  if (item->register_number >= registry->items_space)
    {
      // out of bounds
      return -1;
    }

  if (registry->items[item->register_number] != item)
    {
      // item is not the one expected
      return -2;
    }

  registry->items[item->register_number] = NULL;
  registry->items_count--;

  return 0;
}

struct sfp_item *
sfp_registry_find_by_register (struct sfp_registry * registry,
			       uint16_t register_number)
{
  if (register_number >= registry->items_space)
    return NULL;

  return registry->items[register_number];
}

void
sfp_registry_add_namespace_listener (struct sfp_registry * registry, void
(*listener_cb) (sfp_registry_ns_event_type, struct sfp_item **, size_t, void *),
				     void * ctx)
{
  struct _sfp_registry_ns_listener * listener;

  listener = calloc (1, sizeof(struct _sfp_registry_ns_listener));

  listener->listener_cb = listener_cb;
  listener->ctx = ctx;

  if (registry->_ns_listeners_space < registry->_ns_listeners_count + 1)
    {
      // realloc

      size_t pos = registry->_ns_listeners_space;

      registry->_ns_listeners_space = (registry->_ns_listeners_space * 1.5) + 1;
      registry->_ns_listeners = realloc (registry->_ns_listeners,
					 registry->_ns_listeners_space);

      DEBUG("Re-alloc listener space: %ld", registry->_ns_listeners_space);

      // append

      DEBUG("Appending listener: %ld", pos);

      registry->_ns_listeners[pos] = listener;
      registry->_ns_listeners_count++;
    }
  else
    {
      DEBUG("Inserting listener");

      // insert
      for (size_t i = 0; i < registry->_ns_listeners_space; i++)
	{
	  if (registry->_ns_listeners[i] == NULL)
	    {
	      DEBUG("\tPosition: %ld", i);
	      registry->_ns_listeners[i] = listener;
	      registry->_ns_listeners_count++;
	      break;
	    }
	}
      // TODO: WHOOOOPSIEE("There should have been an empty space - listeners");
    }

  // first event

  struct sfp_item ** items = calloc (registry->items_count,
				     sizeof(struct sfp_item*));
  size_t pos = 0;
  for (size_t i = 0; i < registry->items_space; i++)
    {
      struct sfp_item * item = registry->items[i];
      if (item == NULL)
	continue;

      items[pos] = item;

      pos++;
    }

  // fire
  if (registry->items_count > 0)
    (*listener_cb) (ITEMS_ADDED, items, registry->items_count, ctx);

  free (items);
}

void
sfp_registry_remove_namespace_listener (struct sfp_registry * registry, void
(*listener_cb) (sfp_registry_ns_event_type, struct sfp_item **, size_t, void *),
					void * ctx)
{
  for (size_t i = 0; i < registry->_ns_listeners_space; i++)
    {
      struct _sfp_registry_ns_listener * cl = registry->_ns_listeners[i];

      if (cl == NULL)
	continue;

      if (cl->listener_cb != listener_cb || cl->ctx != ctx)
	continue;

      registry->_ns_listeners[i] = NULL;
      registry->_ns_listeners_count--;

      // remove
      free (cl);
    }
}

void
_sfp_registry_fire_ns_listeners (struct sfp_registry * registry,
				 sfp_registry_ns_event_type event_type,
				 struct sfp_item ** items, size_t numitems)
{
  for (size_t i = 0; i < registry->_ns_listeners_space; i++)
    {
      struct _sfp_registry_ns_listener * cl = registry->_ns_listeners[i];

      if (cl == NULL)
	continue;

      cl->listener_cb (event_type, items, numitems, cl->ctx);
    }
}

void
sfp_registry_lock_read (struct sfp_registry * registry)
{
  // FIXME: implement
}

void
sfp_registry_lock_write (struct sfp_registry * registry)
{
  // FIXME: implement
}

void
sfp_registry_unlock (struct sfp_registry * registry)
{
  // FIXME: implement
}

void
sfp_item_set_write_handler (
    struct sfp_item * item,
    void
    (*handle_write) (
	struct sfp_item * item,
	struct sfp_variant * value,
	int32_t operation_id,
	struct sfp_strategy * strategy,
	void
	(*handle_result) (struct sfp_strategy* strategy, int32_t operation_id,
			  struct sfp_error_information* error),
	void * ctx),
    void * ctx)
{
  item->handle_write_ctx = ctx;
  item->handle_write = handle_write;
}

void
sfp_item_clear_write_handler (struct sfp_item * item)
{
  item->handle_write_ctx = NULL;
  item->handle_write = NULL;
}

void
sfp_item_update_data (struct sfp_item * item, struct sfp_variant * value,
		      uint64_t timestamp)
{
  sfp_variant_set (item->value, value);
  item->timestamp = timestamp == 0L ? sfp_time_in_millis () : timestamp;
  _sfp_item_updated (item);
}

void
sfp_item_update_data_allocated (struct sfp_item * item,
				struct sfp_variant * value, uint64_t timestamp)
{
  if (item->value)
    {
      sfp_variant_free (item->value);
    }

  item->value = value;
  item->timestamp = timestamp == 0L ? sfp_time_in_millis () : timestamp;
  _sfp_item_updated (item);
}

void
_sfp_item_updated (struct sfp_item * item)
{
  /*
   * TODO: should somehow handle updates when we have a more complex
   * communication strategy when acts on changes
   */
}

void
sfp_item_update_data_int32_now (struct sfp_item * item, int32_t value)
{
  sfp_variant_set_int32 (item->value, value);
  item->timestamp = sfp_time_in_millis ();
  _sfp_item_updated (item);
}

void
sfp_item_update_data_int64_now (struct sfp_item * item, int64_t value)
{
  sfp_variant_set_int64 (item->value, value);
  item->timestamp = sfp_time_in_millis ();
  _sfp_item_updated (item);
}

void
sfp_item_update_data_bool_now (struct sfp_item * item, sfp_bool value)
{
  sfp_variant_set_bool (item->value, value);
  item->timestamp = sfp_time_in_millis ();
  _sfp_item_updated (item);
}

void
sfp_item_update_data_double_now (struct sfp_item * item, double value)
{
  sfp_variant_set_double (item->value, value);
  item->timestamp = sfp_time_in_millis ();
  _sfp_item_updated (item);
}

void
sfp_item_update_data_string_copy_now (struct sfp_item * item,
				      const char * value)
{
  sfp_variant_set_string_copy (item->value, value);
  item->timestamp = sfp_time_in_millis ();
  _sfp_item_updated (item);
}

struct sfp_error_information *
sfp_error_information_new (uint16_t code, const char * message)
{
  struct sfp_error_information * error_information = calloc (
      1, sizeof(struct sfp_error_information));

  error_information->error_code = code;

  if (message)
    error_information->error_message = strdup (message);

  return error_information;
}

void
sfp_error_information_free (struct sfp_error_information* error_information)
{
  if (error_information)
    return;

  free (error_information->error_message);
  free (error_information);
}

