/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#include "config.h"

#include <math.h>

/*
 * Private functions
 */

void
_sfp_proto_encode_string (struct evbuffer *data, const char * string);
void
_sfp_proto_encode_string_len (struct evbuffer *data, const char * string,
			      uint16_t len);

/*
 * Implementations
 */

uint8_t marker[] =
  { 0x12, 0x02 };

void
_sfp_proto_encode_data_entry (struct evbuffer *data,
			      struct sfp_data_update_entry * entry)
{
  uint8_t missedUpdates = 0;

  evbuffer_add (data, &entry->register_number, sizeof(uint16_t));
  evbuffer_add (data, &missedUpdates, sizeof(uint8_t));
  evbuffer_add (data, &entry->timestamp, sizeof(uint64_t));
  evbuffer_add (data, &entry->flags, sizeof(uint16_t));
  evbuffer_add (data, &entry->value->type, sizeof(uint8_t));

  DEBUG("encode - registerNumber: %04hx, valueType: %02hhx",
      entry->register_number, entry->value->type);

  switch (entry->value->type)
    {
    case VT_NULL:
      break;
    case VT_BOOLEAN:
      {
	uint8_t flag = entry->value->payload.val_boolean ? 0xFF : 0x00;
	evbuffer_add (data, &flag, sizeof(uint8_t));
	break;
      }
    case VT_INT32:
      {
	evbuffer_add (data, &entry->value->payload.val_int, sizeof(uint32_t));
	break;
      }
    case VT_INT64:
      {
	evbuffer_add (data, &entry->value->payload.val_long, sizeof(uint64_t));
	break;
      }
    case VT_DOUBLE:
      {
	evbuffer_add (data, &entry->value->payload.val_double, sizeof(double));
	break;
      }
    case VT_STRING:
      {
	_sfp_proto_encode_string_len (
	    data, entry->value->payload.val_string,
	    strlen (entry->value->payload.val_string));
	break;
      }
    }
}

void
_sfp_proto_encode_string_len (struct evbuffer *data, const char * string,
			      uint16_t len)
{
  // length
  evbuffer_add (data, &len, sizeof(uint16_t));

  // data - only of present
  if (len > 0 && string != NULL)
    {
      evbuffer_add (data, string, len);
    }
}

uint16_t
cap (size_t value)
{
  if (value > 0xFFFF)
    return 0xFFFF;
  else
    return value;
}

void
_sfp_proto_encode_string (struct evbuffer *data, const char * string)
{
  if (string == NULL)
    {
      _sfp_proto_encode_string_len (data, NULL, 0);
      return;
    }

  uint16_t len = cap (strlen (string));
  _sfp_proto_encode_string_len (data, string, len);
}

int
_sfp_proto_decode_string (struct evbuffer * data, sfp_bool force_big_endian,
			  char ** result)
{
  uint16_t len;
  evbuffer_remove (data, &len, sizeof(uint16_t));

  if (force_big_endian)
    {
      len = be16toh(len);
    }

  if (len == 0)
    return 0;

  *result = malloc (len + 1);
  if (evbuffer_remove (data, *result, len) != len)
    {
      free (*result);
      return -1;
    }
  (*result)[len] = 0;
  return 0;
}

void
_sfp_proto_encode_ns_entry (struct evbuffer *data, struct sfp_item * entry)
{
  uint8_t flags = 0;
  uint8_t data_type = VT_NULL;

  evbuffer_add (data, &entry->register_number, sizeof(uint16_t));
  evbuffer_add (data, &data_type, sizeof(uint8_t));
  evbuffer_add (data, &flags, sizeof(uint8_t));

  _sfp_proto_encode_string (data, entry->item_id);
  _sfp_proto_encode_string (data, entry->description);
  _sfp_proto_encode_string (data, entry->unit);
}

void
_sfp_proto_send_long_message (struct sfp_connection * connection,
			      uint8_t command_code, struct evbuffer * data,
			      sfp_bool is_welcome)
{
  struct evbuffer *output = bufferevent_get_output (connection->bev);

  evbuffer_add (output, &marker, sizeof(marker));
  evbuffer_add (output, &command_code, sizeof(uint8_t));

  if (is_welcome)
    {
      uint16_t size = htobe16(evbuffer_get_length (data));
      evbuffer_add (output, &size, sizeof(uint16_t));
    }
  else
    {
      uint16_t size = evbuffer_get_length (data);
      evbuffer_add (output, &size, sizeof(uint16_t));
    }

  evbuffer_add_buffer (output, data);
  evbuffer_free (data);
}

void
_sfp_proto_send_data_updates (struct sfp_strategy * strategy,
			      struct sfp_data_update_entry * entries,
			      uint16_t count)
{
  struct evbuffer *data = evbuffer_new ();

  evbuffer_add (data, &count, sizeof(uint16_t));

  for (size_t i = 0; i < count; i++)
    {
      _sfp_proto_encode_data_entry (data, &entries[i]);
    }

  _sfp_proto_send_long_message (strategy->connection, MC_DATA_UPDATE, data,
  sfp_false);
}

void
_sfp_proto_send_ns_added (struct sfp_strategy * strategy,
			  struct sfp_item ** items, uint16_t count)
{
  struct evbuffer *data = evbuffer_new ();

  DEBUG("Sending namespace update - %d items:", count);

  evbuffer_add (data, &count, sizeof(uint16_t));

  for (size_t i = 0; i < count; i++)
    {
      _sfp_proto_encode_ns_entry (data, items[i]);
    }

  _sfp_proto_send_long_message (strategy->connection, MC_NS_ADDED, data,
  sfp_false);
}

void
_sfp_proto_send_write_result (struct sfp_strategy * strategy,
			      int32_t operation_id, uint16_t error_code,
			      const char * error_message)
{
  struct evbuffer *data = evbuffer_new ();

  evbuffer_add (data, &operation_id, sizeof(int32_t));
  evbuffer_add (data, &error_code, sizeof(uint16_t));
  _sfp_proto_encode_string (data, error_message);

  _sfp_proto_send_long_message (strategy->connection, MC_WRITE_RESULT, data,
  sfp_false);
}
