/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#include "config.h"

/*
 * Private functions
 */

void
_sfp_server_accept_error_cb (struct evconnlistener *listener, void *ctx);
void
_sfp_server_fire_on_error (struct sfp_server * server, char * message);
void
_sfp_server_accept_conn_cb (struct evconnlistener *listener, evutil_socket_t fd,
			    struct sockaddr *address, int socklen, void *ctx);
void
_sfp_server_bind (struct sfp_server * server, uint16_t port);
void
_sfp_server_unbind (struct sfp_server * server);

/*
 * Implementations
 */

struct sfp_server *
sfp_server_new (struct sfp_registry * registry)
{
  struct sfp_server * server = calloc (1, sizeof(struct sfp_server));

  server->evbase = event_base_new ();
  server->registry = registry;

  return server;
}

void
sfp_server_free (struct sfp_server * server)
{
  event_base_free (server->evbase);
  free (server);
}

void
sfp_server_run (struct sfp_server * server, uint16_t port)
{
  _sfp_server_bind (server, port);
  event_base_dispatch (server->evbase);
  _sfp_server_unbind (server);
}

void
sfp_server_exit_loop (struct sfp_server * server)
{
  event_base_loopexit (server->evbase, NULL);
}

void
_sfp_server_bind (struct sfp_server * server, uint16_t port)
{
  struct sockaddr_in sin;

  memset (&sin, 0, sizeof(sin));
  sin.sin_family = AF_INET;
  sin.sin_addr.s_addr = INADDR_ANY;
  sin.sin_port = htobe16(port);

  server->listener = evconnlistener_new_bind (
      server->evbase, _sfp_server_accept_conn_cb, server,
      LEV_OPT_CLOSE_ON_FREE | LEV_OPT_REUSEABLE,
      -1, (struct sockaddr*) &sin, sizeof(sin));

  if (!server->listener)
    {
      _sfp_server_fire_on_error (server, strerror (errno));
      return;
    }

  evconnlistener_set_error_cb (server->listener, _sfp_server_accept_error_cb);
}

void
_sfp_server_unbind (struct sfp_server *server)
{
  evconnlistener_free (server->listener);
}

void
_sfp_server_fire_on_error (struct sfp_server * server, char * message)
{
  if (server->on_error)
    {
      server->on_error (message);
    }
  else
    {
      fprintf (stderr, "Server failed: %s\n", message);
      fprintf (stderr, "Shutting down.\n");
      fflush (stderr);
      sfp_server_exit_loop (server);
    }
}

void
_sfp_server_accept_conn_cb (struct evconnlistener *listener, evutil_socket_t fd,
			    struct sockaddr *address, int socklen, void *ctx)
{
  struct sfp_server * server = (struct sfp_server*) ctx;
  _sfp_connection_new (server, fd);
}

char *
_sfp_make_socket_error (int error, const char * format)
{
  int len = snprintf (NULL, 0, format, error,
		      evutil_socket_error_to_string(error));
  char * message = malloc (len + 1);
  snprintf (message, len, format, error, evutil_socket_error_to_string(error));

  return message;
}

#define ERROR_FORMAT "Got an error %d (%s) on the listener."

void
_sfp_server_accept_error_cb (struct evconnlistener *listener, void *ctx)
{
  struct sfp_server * server = (struct sfp_server*) ctx;

  int err = EVUTIL_SOCKET_ERROR();

  char * message = _sfp_make_socket_error (err, ERROR_FORMAT);
  _sfp_server_fire_on_error (server, message);
  free (message);
}
