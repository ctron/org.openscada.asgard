/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#include "config.h"

/*
 * Internal functions
 */

void
_sfp_variant_free_data (struct sfp_variant * variant);

void
_sfp_variant_initialize_variant (struct sfp_variant * variant,
				 struct sfp_variant * source);

void
_sfp_variant_initialize_dead (struct sfp_variant * variant);

void
_sfp_variant_initialize_null (struct sfp_variant * variant);

void
_sfp_variant_initialize_int32 (struct sfp_variant * variant, int32_t value);

void
_sfp_variant_initialize_int64 (struct sfp_variant * variant, int64_t value);

void
_sfp_variant_initialize_string (struct sfp_variant * variant, char * value);

void
_sfp_variant_initialize_string_copy (struct sfp_variant * variant, const char * value);

void
_sfp_variant_initialize_bool (struct sfp_variant * variant, sfp_bool value);

void
_sfp_variant_initialize_double (struct sfp_variant * variant, double value);

/*
 * Implementations
 */

struct sfp_variant *
sfp_variant_new_null (char * string)
{
  struct sfp_variant * variant = calloc (1, sizeof(struct sfp_variant));
  _sfp_variant_initialize_null (variant);
  return variant;
}

struct sfp_variant *
sfp_variant_new_string_copy (char * string)
{
  struct sfp_variant * variant = calloc (1, sizeof(struct sfp_variant));
  _sfp_variant_initialize_string_copy (variant, string);
  return variant;
}

struct sfp_variant *
sfp_variant_new_string (char * string)
{
  struct sfp_variant * variant = calloc (1, sizeof(struct sfp_variant));
  _sfp_variant_initialize_string (variant, string);
  return variant;
}

struct sfp_variant *
sfp_variant_new_int32 (int32_t value)
{
  struct sfp_variant * result = calloc (1, sizeof(struct sfp_variant));
  _sfp_variant_initialize_int32 (result, value);
  return result;
}

struct sfp_variant *
sfp_variant_new_int64 (int64_t value)
{
  struct sfp_variant * result = calloc (1, sizeof(struct sfp_variant));
  _sfp_variant_initialize_int64 (result, value);
  return result;
}

struct sfp_variant *
sfp_variant_new_bool (sfp_bool value)
{
  struct sfp_variant * result = calloc (1, sizeof(struct sfp_variant));
  _sfp_variant_initialize_bool (result, value);
  return result;
}

struct sfp_variant *
sfp_variant_new_double (double value)
{
  struct sfp_variant * result = calloc (1, sizeof(struct sfp_variant));
  _sfp_variant_initialize_double (result, value);
  return result;
}

void
_sfp_variant_free_data (struct sfp_variant * variant)
{
  if (variant == NULL)
    return;

  if (variant->flags & VF_ALLOCATED)
    {
      switch (variant->type)
	{
	case VT_STRING:
	  free (variant->payload.val_string);
	  variant->flags = 0; /* clear allocated flag */
	  _sfp_variant_initialize_null(variant);
	  break;
	default:
	  break;
	}
    }
}

void
sfp_variant_free (struct sfp_variant * variant)
{
  if (variant == NULL)
    return;

  _sfp_variant_free_data (variant);

  free (variant);
}

void
sfp_variant_print (struct sfp_variant * value, FILE * stream)
{
  switch (value->type)
    {
    case VT_DEAD:
      fprintf (stream, "[VT_DEAD]");
      break;
    case VT_NULL:
      fprintf (stream, "[VT_NULL]");
      break;
    case VT_INT32:
      fprintf (stream, "[VT_INT32:%d]", value->payload.val_int);
      break;
    case VT_INT64:
      fprintf (stream, "[VT_INT64:%ld]", value->payload.val_long);
      break;
    case VT_DOUBLE:
      fprintf (stream, "[VT_DOUBLE:%f]", value->payload.val_double);
      break;
    case VT_BOOLEAN:
      fprintf (stream, "[VT_BOOLEAN:%d]", value->payload.val_boolean);
      break;
    case VT_STRING:
      fprintf (stream, "[VT_STRING:%s]", value->payload.val_string);
      break;

    }
}

struct sfp_variant *
sfp_variant_copy (struct sfp_variant * value)
{
  struct sfp_variant * result = calloc (1, sizeof(struct sfp_variant));

  _sfp_variant_initialize_variant (result, value);

  return result;
}

void
_sfp_variant_initialize_null (struct sfp_variant * variant)
{
  variant->type = VT_NULL;
}

void
_sfp_variant_initialize_dead (struct sfp_variant * variant)
{
  variant->type = VT_DEAD;
}

void
_sfp_variant_initialize_int32 (struct sfp_variant * variant, int32_t value)
{
  variant->type = VT_INT32;
  variant->payload.val_int = value;
}

void
_sfp_variant_initialize_int64 (struct sfp_variant * variant, int64_t value)
{
  variant->type = VT_INT64;
  variant->payload.val_long = value;
}

void
_sfp_variant_initialize_bool (struct sfp_variant * variant, sfp_bool value)
{
  variant->type = VT_BOOLEAN;
  variant->payload.val_boolean = value;
}

void
_sfp_variant_initialize_double (struct sfp_variant * variant, double value)
{
  variant->type = VT_DOUBLE;
  variant->payload.val_double = value;
}

void
_sfp_variant_initialize_string (struct sfp_variant * variant, char * string)
{
  if (string != NULL)
    {
      variant->type = VT_STRING;
      variant->payload.val_string = string;
    }
  else
    {
      variant->type = VT_NULL;
    }
}

void
_sfp_variant_initialize_string_copy (struct sfp_variant * variant,
				     const char * string)
{
  if (string != NULL)
    {
      variant->type = VT_STRING;
      variant->flags = VF_ALLOCATED;
      variant->payload.val_string = strdup (string);
    }
  else
    {
      variant->type = VT_NULL;
    }
}

void
sfp_variant_set_dead (struct sfp_variant * variant)
{
  _sfp_variant_free_data (variant);
  _sfp_variant_initialize_dead (variant);
}

void
sfp_variant_set_null (struct sfp_variant * variant)
{
  _sfp_variant_free_data (variant);
  _sfp_variant_initialize_null (variant);
}

void
sfp_variant_set_int32 (struct sfp_variant * variant, int32_t value)
{
  _sfp_variant_free_data (variant);
  _sfp_variant_initialize_int32 (variant, value);
}

void
sfp_variant_set_int64 (struct sfp_variant * variant, int64_t value)
{
  _sfp_variant_free_data (variant);
  _sfp_variant_initialize_int64 (variant, value);
}

void
sfp_variant_set_double (struct sfp_variant * variant, double value)
{
  _sfp_variant_free_data (variant);
  _sfp_variant_initialize_double (variant, value);
}

void
sfp_variant_set_string (struct sfp_variant * variant, char * value)
{
  _sfp_variant_free_data (variant);
  _sfp_variant_initialize_string (variant, value);
}

void
sfp_variant_set_string_allocated (struct sfp_variant * variant, char * value)
{
  _sfp_variant_free_data (variant);
  _sfp_variant_initialize_string (variant, value);
  variant->flags |= VF_ALLOCATED;
}

void
sfp_variant_set_string_copy (struct sfp_variant * variant, const char * value)
{
  _sfp_variant_free_data (variant);
  _sfp_variant_initialize_string_copy (variant, value);
}

void
sfp_variant_set_bool (struct sfp_variant * variant, sfp_bool value)
{
  _sfp_variant_free_data (variant);
  _sfp_variant_initialize_bool (variant, value);
}

void
_sfp_variant_initialize_variant (struct sfp_variant * variant,
				 struct sfp_variant * source)
{
  variant->type = source->type;
  variant->flags = source->flags;

  switch (variant->type)
    {
    case VT_NULL:
      break;
    case VT_INT32:
      variant->payload.val_int = source->payload.val_int;
      break;
    case VT_INT64:
      variant->payload.val_long = source->payload.val_long;
      break;
    case VT_DOUBLE:
      variant->payload.val_double = source->payload.val_double;
      break;
    case VT_BOOLEAN:
      variant->payload.val_boolean = source->payload.val_boolean;
      break;
    case VT_STRING:
      {
	if (variant->flags & VF_ALLOCATED)
	  {
	    variant->payload.val_string = strdup (source->payload.val_string);
	  }
	else
	  {
	    variant->payload.val_string = source->payload.val_string;
	  }
	break;
      }

    }
}

void
sfp_variant_set (struct sfp_variant * variant, struct sfp_variant * source)
{
  _sfp_variant_free_data (variant);
  _sfp_variant_initialize_variant (variant, source);
}

int
sfp_variant_get_int32 (struct sfp_variant * variant, int32_t * value)
{
  if (variant == NULL)
    {
      return VARIANT_ERR_NULL;
    }

  switch (variant->type)
    {
    case VT_NULL:
      return VARIANT_ERR_NULL_VALUE;

    case VT_INT32:
      if ( NULL != value)
	{
	  *value = variant->payload.val_int;
	}
      return VARIANT_OK;

    case VT_INT64:
      if ( NULL != value)
	{
	  *value = variant->payload.val_long;
	}
      return VARIANT_OK;

    case VT_BOOLEAN:
      if ( NULL != value)
	{
	  *value = variant->payload.val_boolean ? 1 : 0;
	}
      return VARIANT_OK;

    case VT_DOUBLE:
      if ( NULL != value)
	{
	  *value = variant->payload.val_double;
	}
      return VARIANT_OK;

    case VT_STRING:
      errno = 0;
      int64_t val = strtol (variant->payload.val_string, NULL, 10);
      if ( errno != 0)
	{
	  return VARIANT_ERR_CONVERT;
	}

      if ( NULL != value)
	{
	  *value = val;
	}
      return VARIANT_OK;
    }

  return VARIANT_ERR_CONVERT;
}
