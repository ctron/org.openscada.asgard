## SFP_IEEE_754
## ----------
## According to C99, isnan and isfinite are macros in math.h,
## but some older systems have isnan as a function (possibly as well).
AC_DEFUN([SFP_IEEE_754],
[AC_CHECK_FUNCS([isnan])
AC_CHECK_DECLS([isfinite, isnan], , , [#include <math.h>])
AC_CACHE_CHECK([whether you have IEEE 754 floating-point arithmetic],
               [r_cv_ieee_754],
[if (test "${ac_cv_func_isnan}" = yes \
      || test "${ac_cv_have_decl_isnan}" = yes); then
  r_cv_ieee_754=yes
else
  r_cv_ieee_754=no
fi])
if test "${r_cv_ieee_754}" = yes; then
  AC_DEFINE(IEEE_754, 1,
            [Define if you have IEEE 754 floating point arithmetic.])
else
  AC_MSG_ERROR([IEEE 754 floating-point arithmetic is required])
fi
])# SFP_IEEE_754
