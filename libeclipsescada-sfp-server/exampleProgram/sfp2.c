/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include <eclipsescada/sfp_server.h>

#include "config.h"

#define PORT 9999

void
timer_cb (evutil_socket_t fd, short what, void * ctx)
{
  struct sfp_item * item = (struct sfp_item *) ctx;

  char buffer[512];
  char buffer2[512];

  uint64_t ms = sfp_time_in_millis ();

  time_t t = ms / 1000;
  struct tm tmp;
  localtime_r (&t, &tmp);

  strftime (buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", &tmp);
  snprintf (buffer2, sizeof(buffer2), ".%03d", (int) (ms % 1000));
  strcat (buffer, buffer2);
  sfp_item_update_data_allocated (item, sfp_variant_new_string_copy (buffer),
				  ms);
}

void
memory_cell_write_handler (
    struct sfp_item * item,
    struct sfp_variant * value,
    int32_t operation_id,
    struct sfp_strategy * strategy,
    void
    (*handle_result) (struct sfp_strategy* strategy, int32_t operation_id,
		      struct sfp_error_information* error),
    void * ctx)
{
  INFO("Memory cell write handler");
  sfp_item_update_data (item, value, sfp_time_in_millis ());

  handle_result (strategy, operation_id, NULL);
}

struct sfp_registry * registry;
struct sfp_item * itemx = NULL;

void
item_instance_write_handler (
    struct sfp_item * item,
    struct sfp_variant * variant,
    int32_t operation_id,
    struct sfp_strategy * strategy,
    void
    (*handle_result) (struct sfp_strategy* strategy, int32_t operation_id,
		      struct sfp_error_information* error),
    void * ctx)
{
  INFO("Item instance write handler");

  int32_t value;
  if (sfp_variant_get_int32 (variant, &value)==0)
    {
      if (itemx == NULL)
	{
	  itemx = sfp_registry_register_item (registry, "testX", "Test X Item",
	  NULL);
	  sfp_item_update_data_bool_now (item, sfp_true);
	}
      else
	{
	  sfp_registry_unregister_item (itemx);
	  itemx = NULL;
	  sfp_item_update_data_bool_now (item, sfp_false);
	}
      handle_result (strategy, operation_id, NULL);
    }
  else
    {
      handle_result (
	  strategy, operation_id,
	  sfp_error_information_new (1, "Unable to get int32 value"));
    }
}

int
main (void)
{
  struct sfp_server * server;

  registry = sfp_registry_new ();

  struct sfp_item * item1 = sfp_registry_register_item (registry, "test12",
							"Test 12 Item", NULL);
  sfp_item_update_data_int32_now (item1, 1202);

  struct sfp_item * item2 = sfp_registry_register_item (registry, "test34",
							"Test 34 Item", NULL);
  sfp_item_update_data_int32_now (item2, 1202);

  struct sfp_item * item3 = sfp_registry_register_item (registry, "test56",
							"Test 56 Item", NULL);
  sfp_item_set_write_handler (item3, memory_cell_write_handler, NULL);

  struct sfp_item * item4 = sfp_registry_register_item (registry, "test78",
							"Test 78 Item", NULL);
  sfp_item_set_write_handler (item4, item_instance_write_handler, NULL);

  server = sfp_server_new (registry);

    {
      struct timeval timeout =
	{ 0, 100 * 1000 };
      struct event * timer = event_new (server->evbase, -1, EV_PERSIST,
					timer_cb, item1);
      event_add (timer, &timeout);
    }

    {
      struct timeval timeout =
	{ 5, 0 };
      struct event * timer = event_new (server->evbase, -1, EV_PERSIST,
					timer_cb, item2);
      event_add (timer, &timeout);
    }

  sfp_server_run (server, PORT);

  sfp_server_free (server);
  sfp_registry_free (registry);

  return EXIT_SUCCESS;
}
