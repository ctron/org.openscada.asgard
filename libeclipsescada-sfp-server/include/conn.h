/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#ifndef _CONN_H_
#define _CONN_H_

#include "config.h"

#ifdef __cplusplus
extern "C"
  {
#endif

struct sfp_connection *
_sfp_connection_new (struct sfp_server * server, evutil_socket_t fd);

void
_sfp_connection_free (struct sfp_connection *);

#ifdef __cplusplus
}
#endif

#endif /* _CONN_H_ */
