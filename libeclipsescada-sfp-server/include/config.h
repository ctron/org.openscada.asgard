/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#ifndef CONFIG_H_
#define CONFIG_H_

#include <event2/event.h>
#include <event2/event_struct.h>
#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <event2/listener.h>

#if __BYTE_ORDER == __LITTLE_ENDIAN
#define SFP_LITTLE_ENDIAN
#endif

#include "log.h"

#include <eclipsescada/sfp_server.h>

#include "strategy_readall.h"
#include "proto.h"
#include "conn.h"

#endif /* CONFIG_H_ */
