/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#ifndef _ECLIPSESCADA_SERVER_H_
#define _ECLIPSESCADA_SERVER_H_

#include <eclipsescada/sfp_server.h>

#ifdef __cplusplus
extern "C"
  {
#endif

struct sfp_data_update_entry
{
  uint16_t register_number;
  struct sfp_variant * value;
  uint64_t timestamp;
  uint16_t flags;
};

struct sfp_server
{
  struct event_base * evbase;
  struct evconnlistener *listener;
  struct sfp_registry * registry;

  void
  (*on_error) (char * message);
};

struct sfp_server *
sfp_server_new (struct sfp_registry * registry);

void
sfp_server_free (struct sfp_server * server);

void
sfp_server_run (struct sfp_server * server, uint16_t port);

void
sfp_server_exit_loop (struct sfp_server * server);

#ifdef __cplusplus
}
#endif

#endif /* _ECLIPSESCADA_SERVER_H_ */
