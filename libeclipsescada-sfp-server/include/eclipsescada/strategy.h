/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#ifndef _ECLIPSESCADA_STRATEGY_H_
#define _ECLIPSESCADA_STRATEGY_H_

#include <eclipsescada/sfp_server.h>

#ifdef __cplusplus
extern "C"
  {
#endif

struct sfp_strategy
{
  struct sfp_server * server;
  struct sfp_connection * connection;

  void
  (*handle_hello) (struct sfp_strategy *, struct sfp_message_hello *);

  void
  (*handle_start_browse) (struct sfp_strategy *);
  void
  (*handle_stop_browse) (struct sfp_strategy *);

  void
  (*handle_read_all) (struct sfp_strategy *);
  void
  (*handle_write_command) (struct sfp_strategy *,
			   struct sfp_message_write_command *);

  void
  (*handle_dispose) (struct sfp_strategy *);

};

struct sfp_strategy *
sfp_strategy_new (struct sfp_server * server,
		  struct sfp_connection * connection);

void
sfp_strategy_free (struct sfp_strategy * strategy);

#ifdef __cplusplus
}
#endif

#endif /* _ECLIPSESCADA_STRATEGY_H_ */
