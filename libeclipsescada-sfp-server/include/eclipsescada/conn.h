/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#ifndef _ECLIPSESCADA_CONN_H_
#define _ECLIPSESCADA_CONN_H_

#ifdef __cplusplus
extern "C"
  {
#endif

struct sfp_server;
struct sfp_connection;

#include <eclipsescada/sfp_server.h>

enum sfp_error_codes
{
  SFP_ERROR_OK = 0x0000,
  SFP_ERROR_WRITE_NO_ITEM = 0x0101,
  SFP_ERROR_WRITE_NO_HANDLER = 0x0102
};

struct sfp_connection
{
  struct bufferevent *bev;
  struct event_base *evbase;
  struct sfp_strategy * strategy;
};

#ifdef __cplusplus
}
#endif

#endif /* _ECLIPSESCADA_CONN_H_ */
