/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#ifndef _ECLIPSESCADA_REGISTRY_H_
#define _ECLIPSESCADA_REGISTRY_H_

#ifdef __cplusplus
extern "C"
  {
#endif

struct sfp_item;
struct sfp_registry;

#include <eclipsescada/sfp_server.h>

typedef enum
{
  INPUT = 0x01, OUTPUT = 0x02
} sfp_item_flags;

typedef enum
{
  ITEMS_ADDED = 0x01, ITEMS_REMOVED = 0x02
} sfp_registry_ns_event_type;

struct _sfp_registry_ns_listener
{
  void
  (*listener_cb) (sfp_registry_ns_event_type, struct sfp_item **, size_t,
		  void *);
  void * ctx;
};

struct sfp_registry
{
  struct sfp_item ** items;
  size_t items_space; //< Number of allocated item slots
  size_t items_count; //< Number of allocated items

  struct _sfp_registry_ns_listener ** _ns_listeners;
  size_t _ns_listeners_space;
  size_t _ns_listeners_count;
};

struct sfp_error_information
{
  uint16_t error_code;
  char * error_message;
};

struct sfp_item
{
  uint16_t register_number;
  struct sfp_registry * registry;
  struct sfp_variant * value;
  uint64_t timestamp;
  char * item_id;
  char * description;
  char * unit;

  void
  (*handle_write) (
      struct sfp_item * item,
      struct sfp_variant * value,
      int32_t operation_id,
      struct sfp_strategy * strategy,
      void
      (*handle_result) (struct sfp_strategy* strategy, int32_t operation_id,
			struct sfp_error_information* error),
      void * ctx);

  void * handle_write_ctx;
};

/**
 * Create a new registry instance.
 */
struct sfp_registry *
sfp_registry_new ();

/**
 * Destroy a registry.
 *
 * @note If the registry was added to a server the server must be destroyed first.
 */
void
sfp_registry_free (struct sfp_registry *);

/**
 * Create a new item in the registry.
 *
 * Instead of directly setting the item value the update methods should be used:
 * - ::sfp_item_update_data
 * - ::sfp_item_update_data_allocated
 */
struct sfp_item *
sfp_registry_register_item (struct sfp_registry * registry,
			    const char * item_id, const char * description,
			    const char * unit);

/**
 * Destroy a registered item in the registry.
 */
void
sfp_registry_unregister_item (struct sfp_item * item);

struct sfp_item *
sfp_registry_find_by_register (struct sfp_registry * registry,
			       uint16_t register_number);

/**
 * @brief Set the write handler for an item. If the write handler is not set the item is read-only.
 * @param item the item who's write handler should be set
 * @param ctx a context value which will be passed to the write handler
 *
 * The provided handler method has to ensure that:
 * - The handler_result callback is always called exactly once after the request has been processed
 * - The parameter error of the handler_result callback is either <code>null</code> if there was no error
 * with a provided error_information structure. The error has to be freed by the caller after the call
 * the handler_result has bee completed.
 * - The write handler must pass strategy and operation_id unaltered
 *
 * The write handler will get passed the ctx value that was used when the write handler was set.
 */
void
sfp_item_set_write_handler (
    struct sfp_item * item,
    void
    (*handle_write) (
	struct sfp_item * item,
	struct sfp_variant * value,
	int32_t operation_id,
	struct sfp_strategy * strategy,
	void
	(*handle_result) (struct sfp_strategy* strategy, int32_t operation_id,
			  struct sfp_error_information* error),
	void * ctx),
    void * ctx);

void
sfp_item_clear_write_handler (struct sfp_item * item);

/**
 * The item's value will be set using the provided value.
 *
 * The value is actually assigned to the item. The call must free the provided value.
 *
 * @param the item who's value should be set
 * @param value the value
 * @param timestamp the timestamp of the value
 */
void
sfp_item_update_data (struct sfp_item * item, struct sfp_variant * value,
		      uint64_t timestamp);

/**
 * The item's value will be set using the provided value.
 *
 * The instance of the variant is taken over by the item. The previous variant instance
 * is freed and the new instance is used. The caller must not free the provided value.
 *
 * @param the item who's value should be set
 * @param value the value
 * @param timestamp the timestamp of the value
 */
void
sfp_item_update_data_allocated (struct sfp_item * item,
				struct sfp_variant * value, uint64_t timestamp);

void
sfp_item_update_data_int32_now (struct sfp_item * item, int32_t value);
void
sfp_item_update_data_int64_now (struct sfp_item * item, int64_t value);
void
sfp_item_update_data_double_now (struct sfp_item * item, double value);
void
sfp_item_update_data_bool_now (struct sfp_item * item, sfp_bool value);
void
sfp_item_update_data_string_copy_now (struct sfp_item * item,
				      const char * value);

void
sfp_registry_add_namespace_listener (struct sfp_registry * registry, void
(*listener_cb) (sfp_registry_ns_event_type, struct sfp_item **, size_t, void *),
				     void * ctx);

void
sfp_registry_remove_namespace_listener (struct sfp_registry * registry, void
(*listener_cb) (sfp_registry_ns_event_type, struct sfp_item **, size_t, void *),
					void * ctx);

void
sfp_registry_lock_read (struct sfp_registry * registry);

void
sfp_registry_lock_write (struct sfp_registry * registry);

void
sfp_registry_unlock (struct sfp_registry * registry);

/**
 * Create a new error information
 *
 * If the error message is present, a copy will be made and
 * freed when the error information is freed.
 *
 * @param code the error code
 * @param message the error message (optional)
 */
struct sfp_error_information *
sfp_error_information_new (uint16_t code, const char * message);

void
sfp_error_information_free (struct sfp_error_information* error_information);

#ifdef __cplusplus
}
#endif

#endif /* _ECLIPSESCADA_REGISTRY_H_ */
