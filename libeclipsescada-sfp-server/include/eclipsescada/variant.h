/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#ifndef _ECLIPSESCADA_VARIANT_H_
#define _ECLIPSESCADA_VARIANT_H_

#include <eclipsescada_sfp_server_config.h>

#ifdef __cplusplus
extern "C"
  {
#endif

typedef enum
{
  VT_DEAD = 0x00,
  VT_NULL = 0x01,
  VT_BOOLEAN = 0x02,
  VT_INT32 = 0x03,
  VT_INT64 = 0x04,
  VT_DOUBLE = 0x05,
  VT_STRING = 0x06
} sfp_variant_type;

typedef enum
{
  VF_ALLOCATED = 0x01,
} sfp_variant_flags;

struct sfp_variant
{
  sfp_variant_type type;
  sfp_variant_flags flags;
  union
  {
    uint8_t val_boolean;
    int32_t val_int;
    int64_t val_long;
    double val_double;
    char * val_string; /* FIXME: string should have a length field */
  } payload;
};

struct sfp_variant *
sfp_variant_new_null ();

struct sfp_variant *
sfp_variant_new_string_copy (char * string);

struct sfp_variant *
sfp_variant_new_string (char * string);

struct sfp_variant *
sfp_variant_new_int32 (int32_t value);

struct sfp_variant *
sfp_variant_new_int64 (int64_t value);

struct sfp_variant *
sfp_variant_new_double (double value);

struct sfp_variant *
sfp_variant_new_copy (struct sfp_variant * value);

struct sfp_variant *
sfp_variant_new_bool (sfp_bool value);

void
sfp_variant_free (struct sfp_variant * variant);

void
sfp_variant_print (struct sfp_variant * variant, FILE * stream);

void
sfp_variant_set (struct sfp_variant * variant, struct sfp_variant * source);
void
sfp_variant_set_dead (struct sfp_variant * variant);
void
sfp_variant_set_null (struct sfp_variant * variant);
void
sfp_variant_set_int32 (struct sfp_variant * variant, int32_t value);
void
sfp_variant_set_int64 (struct sfp_variant * variant, int64_t value);
void
sfp_variant_set_bool (struct sfp_variant * variant, sfp_bool value);
void
sfp_variant_set_double (struct sfp_variant * variant, double value);
void
sfp_variant_set_string (struct sfp_variant * variant, char * value);
void
sfp_variant_set_string_allocated (struct sfp_variant * variant, char * value);
void
sfp_variant_set_string_copy (struct sfp_variant * variant, const char * value);

#define VARIANT_OK 0
#define VARIANT_ERR_NULL 1
#define VARIANT_ERR_NULL_VALUE 2
#define VARIANT_ERR_CONVERT 3

int
sfp_variant_get_int32 (struct sfp_variant * variant, int32_t * value);

#ifdef __cplusplus
}
#endif

#endif /* _ECLIPSESCADA_VARIANT_H_ */
