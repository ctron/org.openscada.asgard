/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#ifndef _SFP_SERVER_H_
#define _SFP_SERVER_H_

#include <eclipsescada_sfp_server_config.h>

#ifdef HAVE_ENDIAN_H
#include <endian.h>
#endif
#ifdef HAVE_STDINT_H
#include <stdint.h>
#endif

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <stdarg.h>

#include <arpa/inet.h>

#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

typedef uint8_t sfp_bool;
#define sfp_true 1
#define sfp_false 0

#include <eclipsescada/runtime.h>
#include <eclipsescada/variant.h>
#include <eclipsescada/messages.h>
#include <eclipsescada/conn.h>
#include <eclipsescada/server.h>
#include <eclipsescada/registry.h>
#include <eclipsescada/strategy.h>

#endif /* SFP_SERVER_H_ */
