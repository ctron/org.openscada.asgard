/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#ifndef _ECLIPSESCADA_MESSAGES_H_
#define _ECLIPSESCADA_MESSAGES_H_

#include <eclipsescada/sfp_server.h>

#ifdef __cplusplus
extern "C"
  {
#endif

enum message_codes
{
  MC_HELLO = 0x01,
  MC_WELCOME = 0x02,
  MC_READ_ALL = 0x03,
  MC_DATA_UPDATE = 0x04,
  MC_START_BROWSE = 0x05,
  MC_STOP_BROWSE = 0x06,
  MC_NS_ADDED = 0x07,
  MC_WRITE_COMMAND = 0x08,
  MC_WRITE_RESULT = 0x09
};

struct sfp_message_hello
{
  uint8_t version;
  uint16_t nodeId;
  uint16_t features;
};

struct sfp_message_write_command
{
  uint16_t register_number;
  int32_t operation_id;
  struct sfp_variant * value;
};

#ifdef __cplusplus
}
#endif

#endif /* _ECLIPSESCADA_MESSAGES_H_ */
