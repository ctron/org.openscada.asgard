/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#ifndef _ECLIPSESCADA_RUNTIME_H_
#define _ECLIPSESCADA_RUNTIME_H_

#ifdef __cplusplus
extern "C"
  {
#endif

uint64_t
sfp_time_in_millis ();

#ifdef __cplusplus
}
#endif

#endif /* _ECLIPSESCADA_RUNTIME_H_ */
