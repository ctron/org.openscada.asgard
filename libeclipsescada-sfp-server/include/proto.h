/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#ifndef _PROTO_H_
#define _PROTO_H_

#ifdef __cplusplus
extern "C"
  {
#endif

void
_sfp_proto_encode_entry (struct evbuffer *data,
			 struct sfp_data_update_entry * entry);

void
_sfp_proto_send_long_message (struct sfp_connection * connection,
			      uint8_t command_code, struct evbuffer * data,
			      sfp_bool is_welcome);

void
_sfp_proto_send_data_updates (struct sfp_strategy * strategy,
			      struct sfp_data_update_entry * entries,
			      uint16_t count);

void
_sfp_proto_send_ns_added (struct sfp_strategy * strategy,
			  struct sfp_item ** items, uint16_t count);

void
_sfp_proto_send_write_result (struct sfp_strategy * strategy,
			      int32_t operation_id, uint16_t error_code,
			      const char * error_message);

int
_sfp_proto_decode_string (struct evbuffer * data, sfp_bool force_big_endian,
			  char ** result);

#ifdef __cplusplus
}
#endif

#endif /* _PROTO_H_ */
