/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#ifndef _LOG_H_
#define _LOG_H_

#ifdef LOG_ANSI
#define INFO_START "\e[1m"
#define INFO_END "\e[22m"
#define ERROR_START "\e[1m"
#define ERROR_END "\e[22m"
#else
#define INFO_START ""
#define INFO_END ""
#define ERROR_START ""
#define ERROR_END ""
#endif

#ifdef ENABLE_LOGGING
#define LOG(prefix,suffix,...) do {\
printf(prefix "%s:%d: %s():\t", __FILE__, __LINE__, __FUNCTION__);\
printf(__VA_ARGS__);\
printf(suffix "\n");\
fflush( NULL);\
} while(0)
#else
#define LOG(prefix,suffix,...)
#endif

#define DEBUG(...) LOG("","",__VA_ARGS__)
#define INFO(...) LOG(INFO_START,INFO_END,__VA_ARGS__)
#define ERROR(...) LOG(ERROR_START,ERROR_END,__VA_ARGS__)

#endif
