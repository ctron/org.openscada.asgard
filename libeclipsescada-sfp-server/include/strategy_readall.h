/*******************************************************************************
 * Copyright (c) 2013, 2014 IBH SYSTEMS GmbH and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBH SYSTEMS GmbH - initial API and implementation
 *******************************************************************************/

#ifndef _STRATEGY_READALL_H_
#define _STRATEGY_READALL_H_

#include "config.h"

#ifdef __cplusplus
extern "C"
  {
#endif

void
sfp_strategy_readall_init (struct sfp_strategy * strategy);

#ifdef __cplusplus
}
#endif

#endif /* _STRATEGY_READALL_H_ */
